module CIFAR10Features

using FileIO, JLD2

export  features_d50, features_d100, labels

const PATH_res = joinpath(dirname(@__DIR__), "res")
const NAME_d50 = "features_d50.jld2"
const NAME_d100 = "features_d100.jld2"
const NAME_labels = "labels.jld2"

load_res(name, args...) = load(joinpath(PATH_res, name), args...)
cv64(X) = convert(Matrix{Float64}, X)

features_d50() = cv64(features_d50_float32())
features_d100() = cv64(features_d100_float32())
features_d50_float32() = load_res(NAME_d50, "X")
features_d100_float32() = load_res(NAME_d100, "X")
labels() = load_res(NAME_labels, "labels")

end # module
